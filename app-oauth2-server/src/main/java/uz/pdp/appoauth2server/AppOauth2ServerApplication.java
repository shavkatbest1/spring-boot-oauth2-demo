package uz.pdp.appoauth2server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOauth2ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppOauth2ServerApplication.class, args);
    }

}
